package main

import "gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup"

func main() {
	btrfs_backup.RunBackup()
}
