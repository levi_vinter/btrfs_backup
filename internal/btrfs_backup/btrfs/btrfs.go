package btrfs

import (
	"errors"
	"fmt"
	"log"
	"os/exec"
	"path"
	"strings"

	"github.com/melbahja/goph"
	"gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup/ssh"
)

// Struct to use whenever a btrfs command is run
type Btrfs struct {
	SshClient                 *ssh.SSHClient
	HostConfigSnapshotsPath   string
	RemoteConfigSnapshotsPath string
}

// Check if anything is wrong with a snapshot on the remote host that will be
// used as the parent snapshot in an incremental backup
func (btrfs *Btrfs) CheckParentSnapshot(parentSnapshotId string) error {
	if parentSnapshotId == "0" {
		return nil
	}
	snapshotPath := path.Join(btrfs.RemoteConfigSnapshotsPath, parentSnapshotId, "snapshot")
	snapshotPathIsDirectory, err := btrfs.SshClient.IsDirectory(snapshotPath)
	if err != nil {
		errMessage := fmt.Sprintf("btrfs: check parent snapshot %s: %s", snapshotPath, err.Error())
		return errors.New(errMessage)
	}

	if !snapshotPathIsDirectory {
		errMessage := fmt.Sprintf("btrfs: parent snapshot %s is not a directory", snapshotPath)
		return errors.New(errMessage)
	}
	subvolumeDetails := btrfs.SshClient.ShowSubolume(snapshotPath)

	for _, details := range subvolumeDetails {
		columns := strings.Split(details, ":")
		if strings.TrimSpace(columns[0]) != "Flags" {
			continue
		}

		if strings.TrimSpace(columns[1]) != "readonly" {
			errMessage := fmt.Sprintf(
				"btrfs: parent snapshot %s on remote is not readonly",
				snapshotPath,
			)
			return errors.New(errMessage)
		}

		return nil
	}

	errMessage := fmt.Sprintf("parent snapshot %s on remote is in a failed state", snapshotPath)

	return errors.New(errMessage)
}

// Return a btrfs send command
func (btrfs *Btrfs) sendCommand(snapshotId string, parentSnapshotId string) *exec.Cmd {
	if parentSnapshotId != "0" || parentSnapshotId == "" {
		return btrfs.incrementalSendCommand(snapshotId, parentSnapshotId)
	}

	command := exec.Command(
		"btrfs",
		"send",
		path.Join(btrfs.HostConfigSnapshotsPath, snapshotId, "snapshot"),
	)

	return command
}

// Return a btrfs send command that uses a parent snapshot
func (btrfs *Btrfs) incrementalSendCommand(snapshotId string, parentSnapshotId string) *exec.Cmd {
	command := exec.Command(
		"btrfs",
		"send",
		"-p",
		path.Join(btrfs.HostConfigSnapshotsPath, parentSnapshotId, "snapshot"),
		path.Join(btrfs.HostConfigSnapshotsPath, snapshotId, "snapshot"),
	)

	return command
}

// Log btrfs send command to inform user of what is going on
func (btrfs *Btrfs) logSendCommand(sendCommand *exec.Cmd, receiveCommand *goph.Cmd) {
	log.Printf(
		"%s | ssh %s@%s \"%s\"",
		sendCommand.String(),
		btrfs.SshClient.Config.User,
		btrfs.SshClient.Config.Host,
		receiveCommand.String(),
	)
}

// Send btrfs to remote host
func (btrfs *Btrfs) Send(snapshotId string, parentSnapshotId string) error {
	sendCommand := btrfs.sendCommand(snapshotId, parentSnapshotId)
	remoteSnapshotPath := path.Join(btrfs.RemoteConfigSnapshotsPath, snapshotId)
	btrfs.SshClient.CreateDir(remoteSnapshotPath)
	receiveCommand := btrfs.SshClient.BtrfsReceiveCommand(remoteSnapshotPath)

	sendCommandStdoutPipe, err := sendCommand.StdoutPipe()
	if err != nil {
		errMessage := fmt.Sprintf("btrfs: send command output: %s", err.Error())
		return errors.New(errMessage)
	}
	receiveCommand.Stdin = sendCommandStdoutPipe

	err = sendCommand.Start()
	if err != nil {
		return fmt.Errorf("btrfs: send output: %s", err.Error())
	}

	btrfs.logSendCommand(sendCommand, receiveCommand)

	out, err := receiveCommand.CombinedOutput()
	if err != nil {
		return fmt.Errorf("btrfs: receive command output: %s", string(out))
	}

	return nil
}
