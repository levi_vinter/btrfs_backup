package snapper

import (
	"encoding/json"
	"errors"
	"fmt"
	"os/exec"
)

// Snapper config returned by command `snapper --jsonout list-configs`
type SnapperConfigs struct {
	Configs []SnapperConfig `json:"configs"`
}

type SnapperConfig struct {
	Config    string `json:"config"`
	Subvolume string `json:"subvolume"`
}

// List all snapper configs on system
func listSnapperConfigs() (*SnapperConfigs, error) {
	snapperOutput, err := exec.Command(
		"snapper", "--jsonout", "list-configs",
	).Output()
	if err != nil {
		errMessage := fmt.Sprintf(
			"snapper --jsonout listconfigs: %s",
			snapperOutput,
		)
		return nil, errors.New(errMessage)
	}

	var snapperConfigs SnapperConfigs
	err = json.Unmarshal(snapperOutput, &snapperConfigs)
	if err != nil {
		errMessage := fmt.Sprintf(
			"snapper --jsonout listconfigs: unknown output: %s",
			snapperOutput,
		)
		return nil, errors.New(errMessage)
	}

	return &snapperConfigs, nil
}

// Return snapper subvolume paths mapped by snapper config
func GetSubvolumes(userProvidedSnapperConfigs []string) (map[string]string, error) {
	var subvolumesByConfig = make(map[string]string)
	for _, snapperConfig := range userProvidedSnapperConfigs {
		subvolumesByConfig[snapperConfig] = ""
	}

	snapperConfigs, err := listSnapperConfigs()
	if err != nil {
		return nil, err
	}
	for _, snapperConfig := range snapperConfigs.Configs {
		_, exists := subvolumesByConfig[snapperConfig.Config]
		if !exists {
			continue
		}

		if snapperConfig.Subvolume == "" {
			errorMessage := fmt.Sprintf("no subvolume found for config %s", snapperConfig.Config)
			return subvolumesByConfig, errors.New(errorMessage)
		}

		subvolumesByConfig[snapperConfig.Config] = snapperConfig.Subvolume
	}

	return subvolumesByConfig, nil
}

func CleanUpSnapshots() error {
	output, err := exec.Command("systemctl", "start", "snapper-cleanup.service").Output()
	if err != nil {
		errMessage := fmt.Sprintf(
			"snapshot cleanup failed: %s: %s",
			"systemctl start snapper-cleanup.service",
			output,
		)
		return errors.New(errMessage)
	}

	return nil
}
