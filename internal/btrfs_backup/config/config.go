package config

import (
	"encoding/json"
	"errors"
	"fmt"
	"os"
)

// JSON config provided by user of btrfs_backup
type UserConfig struct {
	SSH            SSHConfig `json:"ssh"`
	SnapperConfigs []string  `json:"snapperConfigs"`
}

type SSHConfig struct {
	User       string `json:"user"`
	Host       string `json:"host"`
	RemotePath string `json:"remotePath"`
	Port       uint   `json:"port"`
	PrivateKey string `json:"privateKey"`
	KnownHosts string `json:"knownHosts"`
}

func NewConfig() (*UserConfig, error) {
	configFilePath := "/etc/btrfs_backup/config.json"

	fileContent, err := os.ReadFile(configFilePath)
	if err != nil {
		errorMessage := fmt.Sprintf("json file %s not found", configFilePath)
		return nil, errors.New(errorMessage)
	}

	var userConfig UserConfig
	err = json.Unmarshal(fileContent, &userConfig)
	if err != nil {
		return nil, errors.New("json file is not valid")
	}

	return &userConfig, nil
}
