package btrfs_backup

import (
	"fmt"
	"log"
	"os/exec"
	"os/user"
	"path"
	"sort"
	"strconv"
	"strings"

	"gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup/btrfs"
	"gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup/config"
	"gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup/snapper"
	"gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup/ssh"
)

func checkIfRoot() {
	user, err := user.Current()
	if err != nil {
		errMessage := fmt.Sprintf("current user: %s", err.Error())
		log.Fatal(errMessage)
	}

	username := user.Username
	if username != "root" {
		log.Fatalln("you must be root")
	}
}

// Check if snapper config directory exist on remote host
func configExistsOnRemote(snapperConfig string, configsOnRemote []string) bool {
	for _, configOnRemote := range configsOnRemote {
		if snapperConfig == configOnRemote {
			return true
		}
	}

	return false
}

// List all snapshots on hosts
func listHostSnapshots(snapshotPath string) []string {
	out, err := exec.Command("ls", path.Join(snapshotPath, ".snapshots")).Output()
	if err != nil {
		lsCommand := fmt.Sprintf("ls %s", path.Join(snapshotPath, ".snapshots"))
		log.Fatalf("%s, %s", lsCommand, out)
	}

	snapshots := strings.Split(strings.TrimRight(string(out), "\n"), "\n")

	return snapshots
}

// Return only snapshot id:s that where found on host but not on remote host
// The second return value is a snapshot ID that can be used as parent ID for
// an incremental backup
func filterSnapshotIdsNotOnRemote(hostSnapshotIds []string, remoteLatestId int) ([]int, int) {
	snapshotIdsNotOnRemote := make([]int, 0, len(hostSnapshotIds))

	if remoteLatestId == 0 {
		for _, hostSnapshotId := range hostSnapshotIds {
			hostSnapshotIdInteger, _ := strconv.Atoi(hostSnapshotId)

			snapshotIdsNotOnRemote = append(snapshotIdsNotOnRemote, hostSnapshotIdInteger)
		}
		sort.Ints(snapshotIdsNotOnRemote)

		return snapshotIdsNotOnRemote, 0
	}

	var sharedSnapshotParent int
	for _, hostSnapshotId := range hostSnapshotIds {
		hostSnapshotIdInteger, _ := strconv.Atoi(hostSnapshotId)
		if remoteLatestId == hostSnapshotIdInteger {
			sharedSnapshotParent = hostSnapshotIdInteger
		}
		if hostSnapshotIdInteger <= remoteLatestId {
			continue
		}
		snapshotIdsNotOnRemote = append(snapshotIdsNotOnRemote, hostSnapshotIdInteger)
	}

	sort.Ints(snapshotIdsNotOnRemote)

	return snapshotIdsNotOnRemote, sharedSnapshotParent
}

// Backup snapshots for a given snapper config
func backupSnapperConfigSnapshot(
	sshClient *ssh.SSHClient,
	subvolume string,
	snapperConfig string,
	remotePath string,
) {
	log.Printf("Starting backing up %s", subvolume)

	remoteConfigSnapshotsPath := path.Join(remotePath, snapperConfig, ".snapshots")

	snaphshotIdsOnRemote, err := sshClient.List(remoteConfigSnapshotsPath)
	if err != nil {
		log.Fatal(err.Error())
	}
	var remoteLatestSnapshotId int
	for index, snapshotId := range snaphshotIdsOnRemote {
		snapshotIdInteger, _ := strconv.Atoi(snapshotId)
		if index == 0 || snapshotIdInteger > remoteLatestSnapshotId {
			remoteLatestSnapshotId = snapshotIdInteger
		}
	}

	hostSnapshotIds := listHostSnapshots(subvolume)

	snapshotsNotOnRemote, sharedSnapshotParent := filterSnapshotIdsNotOnRemote(
		hostSnapshotIds,
		remoteLatestSnapshotId,
	)
	parentSnapshotId := strconv.Itoa(sharedSnapshotParent)

	hostSnapshotPath := path.Join(subvolume, ".snapshots")
	btrfs := btrfs.Btrfs{
		SshClient:                 sshClient,
		HostConfigSnapshotsPath:   hostSnapshotPath,
		RemoteConfigSnapshotsPath: remoteConfigSnapshotsPath,
	}
	err = btrfs.CheckParentSnapshot(parentSnapshotId)
	if err != nil {
		log.Fatal(err.Error())
	}

	if len(snapshotsNotOnRemote) == 0 {
		log.Printf("No snapshots to backup for %s", subvolume)
		return
	}

	snapshotId := strconv.Itoa(snapshotsNotOnRemote[len(snapshotsNotOnRemote)-1])
	err = btrfs.Send(snapshotId, parentSnapshotId)
	if err != nil {
		log.Fatal(err.Error())
	}

	log.Printf("Finished backing up %s", subvolume)
}

// Run backups
func RunBackup() {
	checkIfRoot()
	config, err := config.NewConfig()
	if err != nil {
		log.Fatal(err.Error())
	}
	subvolumesMappedByConfig, err := snapper.GetSubvolumes(
		config.SnapperConfigs,
	)
	if err != nil {
		log.Fatal(err.Error())
	}

	sshClient := ssh.SSHClient{Config: config.SSH}
	err = sshClient.StartConnection()
	if err != nil {
		log.Fatalf("ssh connection: %s", err.Error())
	}
	defer sshClient.CloseConnection()

	remotePath := config.SSH.RemotePath
	isBtrfs, err := sshClient.RemotePathIsBtrfsFileSystem(remotePath)
	if err != nil {
		log.Fatalf("ssh mount output: %s", err.Error())
	}
	if !isBtrfs {
		log.Fatalf("%s: remote path is not a btrfs filesystem", remotePath)
	}
	configsOnRemote, err := sshClient.List(remotePath)
	if err != nil {
		log.Fatalf(err.Error())
	}

	for config, subvolume := range subvolumesMappedByConfig {
		if !configExistsOnRemote(config, configsOnRemote) {
			err = sshClient.CreateDir(path.Join(remotePath, config, ".snapshots"))
			if err != nil {
				log.Fatalf(err.Error())
			}
		}

		backupSnapperConfigSnapshot(&sshClient, subvolume, config, remotePath)
	}

	err = snapper.CleanUpSnapshots()
	if err != nil {
		log.Fatal(err.Error())
	}
}
