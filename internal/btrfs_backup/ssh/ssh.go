package ssh

import (
	"errors"
	"fmt"
	"log"
	"path"
	"strings"

	"github.com/melbahja/goph"
	"gitlab.com/levi_vinter/btrfs_backup/internal/btrfs_backup/config"
	"golang.org/x/crypto/ssh"
)

// Client to use whenever an SSH command is run
type SSHClient struct {
	connection *goph.Client
	Config     config.SSHConfig
}

// Run SSH command
func (sshClient *SSHClient) RunCommand(command string) (string, error) {
	out, err := sshClient.connection.Run(command)

	return string(out), err
}

// Start SSH connection to remote host
func (sshClient *SSHClient) StartConnection() error {
	auth, err := goph.Key(sshClient.Config.PrivateKey, "")
	if err != nil {
		return err
	}

	client, err := goph.NewConn(&goph.Config{
		User:     sshClient.Config.User,
		Addr:     sshClient.Config.Host,
		Port:     sshClient.Config.Port,
		Auth:     auth,
		Timeout:  goph.DefaultTimeout,
		Callback: ssh.InsecureIgnoreHostKey(),
	})
	if err != nil {
		return err
	}

	sshClient.connection = client

	return nil
}

// Close SSH connection to remote host
func (sshClient *SSHClient) CloseConnection() {
	sshClient.connection.Close()
}

// Check if remote host path is a btrfs file system
func (sshClient *SSHClient) RemotePathIsBtrfsFileSystem(remotePath string) (bool, error) {
	mountOutput, err := sshClient.RunCommand("mount | grep -i btrfs")
	if err != nil {
		errorMessage := fmt.Sprintf("ssh: mount | grep -i btrfs: %s", mountOutput)
		return false, errors.New(errorMessage)
	}

	if mountOutput == "" {
		return false, nil
	}

	mounts := strings.Split(strings.TrimRight(mountOutput, "\n"), "\n")
	for _, mount := range mounts {
		mountColumns := strings.Split(
			strings.TrimRight(mount, "\n"),
			" ",
		)
		if len(mountColumns) < 3 {
			continue
		}

		filesystem := mountColumns[2]
		if filesystem == path.Dir(remotePath) {
			return true, nil
		}
	}

	return false, nil
}

// List directories on remote host path
func (sshClient *SSHClient) List(remotePath string) ([]string, error) {
	var dirs []string
	out, err := sshClient.RunCommand("ls " + remotePath)
	if err != nil {
		errMessage := fmt.Sprintf("ssh: ls %s output: %s", remotePath, out)
		return dirs, errors.New(errMessage)
	}

	dirs = strings.Split(strings.TrimRight(string(out), "\n"), "\n")

	return dirs, nil
}

// Create directory on remote host
func (sshClient *SSHClient) CreateDir(configDirPath string) error {
	out, err := sshClient.RunCommand("sudo mkdir -p " + configDirPath)
	if err != nil {
		errMessage := fmt.Sprintf("ssh: sudo mkdir -p %s: %s", configDirPath, out)
		return errors.New(errMessage)
	}

	return nil
}

// Check if path on remote host is a directory
func (sshClient *SSHClient) IsDirectory(remotePath string) (bool, error) {
	command := "test -d " + remotePath + " && echo 'true' || echo 'false'"
	out, err := sshClient.RunCommand(command)
	if err != nil {
		errMessage := fmt.Sprintf("ssh: %s: %s", command, out)
		return false, errors.New(errMessage)
	}

	isDirectory := strings.TrimRight(string(out), "\n") == "true"

	return isDirectory, nil
}

// Return btrfs subvolume info from remote host
func (sshClient *SSHClient) ShowSubolume(snapshotPath string) []string {
	out, err := sshClient.RunCommand(
		"sudo btrfs subvolume show " + snapshotPath,
	)
	if err != nil {
		log.Fatalln(string(out))
	}

	result := strings.Split(
		strings.TrimRight(string(out), "\n"),
		"\n",
	)

	return result
}

// Run btrfs receive command on remote host
func (sshClient *SSHClient) BtrfsReceiveCommand(path string) *goph.Cmd {
	command, _ := sshClient.connection.Command("sudo", "btrfs", "receive", path)

	return command
}
