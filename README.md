# btrfs_backup

## Backup up your btrfs snapshots

This program will backup your btrfs snapshots to a remote host through SSH.

It will assume you're are using ```snapper``` to create your snapshots. If not this program won't work.

The program must be run as root in order to run the ```btrfs``` and ```snapper``` commands.

The user on the remote host must have sudo permission without having to provide a password.

## Config file

To run the program you have to create a JSON config file in ```/etc/btrfs_backup/config.json```.

The config file should look like following:

```json
{
    "ssh": {
        "user": "user",
        "host": "example.com",
        "port": 22,
        "remotePath": "/path/to/backup/mount",
        "privateKey": "/home/user/.ssh/id_ed25519",
        "knownHosts": "/home/user/.ssh/known_hosts"
    },
    "snapperConfigs": ["home", "root"]
}
```
