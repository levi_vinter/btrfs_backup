module gitlab.com/levi_vinter/btrfs_backup

go 1.18

require github.com/melbahja/goph v1.2.1

require (
	github.com/kr/fs v0.1.0 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/pkg/sftp v1.12.0 // indirect
	golang.org/x/crypto v0.0.0-20201217014255-9d1352758620 // indirect
	golang.org/x/sys v0.0.0-20201218084310-7d0127a74742 // indirect
)
